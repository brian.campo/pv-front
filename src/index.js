import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from '@material-ui/styles';
import { CssBaseline } from '@material-ui/core';

import App from './pages/App';
import Themes from './themes';
import { LayoutProvider } from './context/LayoutContext';

ReactDOM.render(
  <LayoutProvider>
    <ThemeProvider theme={Themes.default}>
      <CssBaseline />
      <App />
    </ThemeProvider>
  </LayoutProvider>,

  document.getElementById('root')
);
