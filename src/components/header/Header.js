import React, { useState } from 'react';
import classNames from 'classnames';

import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  InputBase,
  Badge,
  Menu,
  MenuItem,
  Fab
} from '@material-ui/core';
import {
  ArrowBack as ArrowBackIcon,
  Menu as MenuIcon,
  Search as SearchIcon,
  Notifications as NotificationsIcon,
  Mail as MailIcon,
  Send as SendIcon,
  Person as AccountIcon
} from '@material-ui/icons';

import Notification from '../Notification/Notification';
import UserAvatar from '../UserAvatar/UserAvatar';

import useStyles from './styles';

import { useLayoutState, useLayoutDispatch, toggleSidebar } from '../../context/LayoutContext';

const notifications = [
  { id: 0, color: 'warning', type: 'offer', message: 'Check out this awesome ticket' },
  { id: 1, color: 'success', type: 'info', message: 'We did it' },
  { id: 2, color: 'secondary', type: 'notification', message: ' a simple notification' },
  { id: 3, color: 'primary', type: 'e-commerce', message: '12 new orders ' }
];

const messages = [
  {
    id: 0,
    color: 'warning',
    name: 'Jane Hew',
    message: 'Hey! How is it going?',
    time: '9:32',
    variant: 'contained'
  },
  { id: 1, color: 'success', name: 'Lloyd Brown', message: 'Check out Dashboard', time: '9:18' },
  { id: 2, color: 'primary', name: 'Mark Winstn', message: 'Setup appointment', time: '9:15' },
  { id: 3, color: 'secondary', name: 'Liana Dutti', message: 'Good news', time: '9:09' }
];

function Header() {
  const classes = useStyles();

  const layoutState = useLayoutState();
  const layoutDispatch = useLayoutDispatch();

  const [isSearchOpen, setSearchOpen] = useState(false);
  const [isNotificationsUnread, setIsNotificationsUnread] = useState(true);
  const [notificationsMenu, setNotificationsMenu] = useState(null);
  const [isMailUnread, setIsMailUnread] = useState(true);
  const [mailMenu, setMailMenu] = useState(null);
  const [profileMenu, setProfileMenu] = useState(null);

  return (
    <div>
      <AppBar position='fixed' className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          <IconButton
            color='inherit'
            onClick={() => toggleSidebar(layoutDispatch)}
            className={classNames(classes.headerMenuButton, classes.headerMenuButtonCollapse)}
          >
            {layoutState.isSidebarOpened ? (
              <ArrowBackIcon
                classes={{ root: classNames(classes.headerIcon, classes.headerIconCollapse) }}
              />
            ) : (
              <MenuIcon
                classes={{ root: classNames(classes.headerIcon, classes.headerIconCollapse) }}
              />
            )}
          </IconButton>
          <Typography variant='h6' weight='medium' className={classes.logotype}>
            PV Processor Admin Dashboard
          </Typography>
          <div className={classes.grow} />
          <div
            className={classNames(classes.search, {
              [classes.searchFocused]: isSearchOpen
            })}
          >
            <div
              role='searchbox'
              tabIndex='0'
              className={classNames(classes.searchIcon, {
                [classes.searchIconOpened]: isSearchOpen
              })}
              onClick={() => setSearchOpen(!isSearchOpen)}
              onKeyPress={() => setSearchOpen(!isSearchOpen)}
            >
              <SearchIcon classes={{ root: classes.headerIcon }} />
            </div>
            <InputBase
              placeholder='Search…'
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput
              }}
            />
          </div>
          {/* Notifications */}
          <IconButton
            color='inherit'
            aria-haspopup='true'
            aria-controls='mail-menu'
            onClick={e => {
              setNotificationsMenu(e.currentTarget);
              setIsNotificationsUnread(false);
            }}
            className={classes.headerMenuButton}
          >
            <Badge
              badgeContent={isNotificationsUnread ? notifications.length : null}
              color='secondary'
            >
              <NotificationsIcon classes={{ root: classes.headerIcon }} />
            </Badge>
          </IconButton>

          <Menu
            id='notifications-menu'
            open={Boolean(notificationsMenu)}
            anchorEl={notificationsMenu}
            onClose={() => setNotificationsMenu(null)}
            className={classes.headerMenu}
            disableAutoFocusItem
          >
            {notifications.map(notification => (
              <MenuItem
                key={notification.id}
                onClick={() => setNotificationsMenu(null)}
                className={classes.headerMenuItem}
              >
                <Notification {...notification} typographyVariant='inherit' />
              </MenuItem>
            ))}
          </Menu>

          {/* Mail */}
          <IconButton
            color='inherit'
            aria-haspopup='true'
            aria-controls='mail-menu'
            onClick={e => {
              setMailMenu(e.currentTarget);
              setIsMailUnread(false);
            }}
            className={classes.headerMenuButton}
          >
            <Badge badgeContent={isMailUnread ? messages.length : null} color='secondary'>
              <MailIcon classes={{ root: classes.headerIcon }} />
            </Badge>
          </IconButton>

          <Menu
            id='mail-menu'
            open={Boolean(mailMenu)}
            anchorEl={mailMenu}
            onClose={() => setMailMenu(null)}
            MenuListProps={{ className: classes.headerMenuList }}
            className={classes.headerMenu}
            classes={{ paper: classes.profileMenu }}
            disableAutoFocusItem
          >
            <div className={classes.profileMenuUser}>
              <Typography variant='h4' weight='medium'>
                New Messages
              </Typography>
              <Typography className={classes.profileMenuLink} component='a' color='secondary'>
                {messages.length} New Messages
              </Typography>
            </div>
            {messages.map(message => (
              <MenuItem key={message.id} className={classes.messageNotification}>
                <div className={classes.messageNotificationSide}>
                  <UserAvatar color={message.color} name={message.name} />
                  <Typography size='sm' color='text' colorBrightness='secondary'>
                    {message.time}
                  </Typography>
                </div>
                <div
                  className={classNames(
                    classes.messageNotificationSide,
                    classes.messageNotificationBodySide
                  )}
                >
                  <Typography weight='medium' gutterBottom>
                    {message.name}
                  </Typography>
                  <Typography color='text' colorBrightness='secondary'>
                    {message.message}
                  </Typography>
                </div>
              </MenuItem>
            ))}
            <Fab
              variant='extended'
              color='primary'
              aria-label='Add'
              className={classes.sendMessageButton}
            >
              Send New Message
              <SendIcon className={classes.sendButtonIcon} />
            </Fab>
          </Menu>

          {/* UserInfo */}
          <IconButton
            aria-haspopup='true'
            color='inherit'
            className={classes.headerMenuButton}
            aria-controls='profile-menu'
            onClick={e => setProfileMenu(e.currentTarget)}
          >
            <AccountIcon classes={{ root: classes.headerIcon }} />
          </IconButton>

          <Menu
            id='profile-menu'
            open={Boolean(profileMenu)}
            anchorEl={profileMenu}
            onClose={() => setProfileMenu(null)}
            className={classes.headerMenu}
            classes={{ paper: classes.profileMenu }}
            disableAutoFocusItem
          >
            <div className={classes.profileMenuUser}>
              <Typography variant='h4' weight='medium'>
                Joe User
              </Typography>
              <Typography
                className={classes.profileMenuLink}
                component='a'
                color='primary'
                href='https://joeuser.com'
              >
                JoeUser.com
              </Typography>
            </div>
            <MenuItem className={classNames(classes.profileMenuItem, classes.headerMenuItem)}>
              <AccountIcon className={classes.profileMenuIcon} /> Profile
            </MenuItem>
            <MenuItem className={classNames(classes.profileMenuItem, classes.headerMenuItem)}>
              <AccountIcon className={classes.profileMenuIcon} /> Tasks
            </MenuItem>
            <MenuItem className={classNames(classes.profileMenuItem, classes.headerMenuItem)}>
              <AccountIcon className={classes.profileMenuIcon} /> Messages
            </MenuItem>
            <div className={classes.profileMenuUser}>
              <Typography
                className={classes.profileMenuLink}
                color='primary'
                // onClick={() => signOut(userDispatch, props.history)}
              >
                Sign Out
              </Typography>
            </div>
          </Menu>
        </Toolbar>
      </AppBar>
    </div>
  );
}

Header.propTypes = {};

export default Header;
