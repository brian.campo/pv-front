import React from 'react';
import { Router, Location } from '@reach/router';
import classnames from 'classnames';

import Temp from '../../pages/Temp';
import Error from '../../pages/error/Error';
import Header from '../Header/Header';
import Sidebar from '../sidebar/Sidebar';

import useStyles from './styles';

import { useLayoutState } from '../../context/LayoutContext';

function Layout() {
  const classes = useStyles();

  const layoutState = useLayoutState();

  return (
    <div className={classes.root}>
      <Header />
      <Location>
        {locationProps => <Sidebar location={locationProps && locationProps.location} />}
      </Location>
      <div
        className={classnames(classes.content, {
          [classes.contentShift]: layoutState.isSidebarOpened
        })}
      >
        <div className={classes.fakeToolbar} />
        <Router>
          <Temp path='temp' msg='test' />
          <Error default />
        </Router>
      </div>
    </div>
  );
}

export default Layout;
