import React from 'react';
import PropTypes from 'prop-types';
import Temp from '../../pages/Temp';

function PrivateRoute(props) {
  const { component: Component, path } = props;
  const isAuthenticated = true;
  return isAuthenticated ? <Component path={path} msg='authenticated' /> : <Temp msg={path} />;
}

PrivateRoute.propTypes = {
  component: PropTypes.func.isRequired,
  path: PropTypes.string.isRequired
};

export default PrivateRoute;
