import React from 'react';
import PropTypes from 'prop-types';

import { Button, Typography } from '@material-ui/core';
import {
  NotificationsNone as NotificationsIcon,
  ThumbUp as ThumbUpIcon,
  ShoppingCart as ShoppingCartIcon,
  LocalOffer as TicketIcon,
  BusinessCenter as DeliveredIcon,
  SmsFailed as FeedbackIcon,
  DiscFull as DiscIcon,
  Email as MessageIcon,
  Report as ReportIcon,
  Error as DefenceIcon,
  AccountBox as CustomerIcon,
  Done as ShippedIcon,
  Publish as UploadIcon
} from '@material-ui/icons';
import { useTheme } from '@material-ui/styles';
import classnames from 'classnames';
import tinycolor from 'tinycolor2';

// styles
import useStyles from './styles';

const typesIcons = {
  'e-commerce': <ShoppingCartIcon />,
  notification: <NotificationsIcon />,
  offer: <TicketIcon />,
  info: <ThumbUpIcon />,
  message: <MessageIcon />,
  feedback: <FeedbackIcon />,
  customer: <CustomerIcon />,
  shipped: <ShippedIcon />,
  delivered: <DeliveredIcon />,
  defence: <DefenceIcon />,
  report: <ReportIcon />,
  upload: <UploadIcon />,
  disc: <DiscIcon />
};

// ####################################################################
function getIconByType(type = 'offer') {
  return typesIcons[type];
}

// ####################################################################
export default function Notification(props) {
  const {
    variant,
    color,
    type,
    typographyVariant,
    className,
    message,
    shadowless,
    extraButton,
    extraButtonClick
  } = props;

  var classes = useStyles();
  var theme = useTheme();

  const icon = getIconByType(type);
  const iconWithStyles = React.cloneElement(icon, {
    classes: {
      root: classes.notificationIcon
    },
    style: {
      color: theme.palette[color] && theme.palette[color].main
    }
  });
  console.log('iconWithStyles', iconWithStyles);

  return (
    <div
      className={classnames(classes.notificationContainer, className, {
        [classes.notificationContained]: variant === 'contained',
        [classes.notificationContainedShadowless]: shadowless
      })}
      style={{
        backgroundColor:
          variant === 'contained' && theme.palette[color] && theme.palette[color].main
      }}
    >
      <div
        className={classnames(classes.notificationIconContainer, {
          [classes.notificationIconContainerContained]: variant === 'contained',
          [classes.notificationIconContainerRounded]: variant === 'rounded'
        })}
        style={{
          backgroundColor:
            variant === 'rounded' &&
            theme.palette[color] &&
            tinycolor(theme.palette[color].main)
              .setAlpha(0.15)
              .toRgbString()
        }}
      >
        {iconWithStyles}
      </div>
      <div className={classes.messageContainer}>
        <Typography
          className={classnames({
            [classes.containedTypography]: variant === 'contained'
          })}
          variant={typographyVariant}
          size={variant !== 'contained' && !typographyVariant && 'md'}
        >
          {message}
        </Typography>
        {extraButton && extraButtonClick && (
          <Button onClick={extraButtonClick} disableRipple className={classes.extraButton}>
            {extraButton}
          </Button>
        )}
      </div>
    </div>
  );
}

Notification.defaultProps = {
  variant: '',
  className: '',
  extraButton: null,
  extraButtonClick: null,
  shadowless: false
};

Notification.propTypes = {
  variant: PropTypes.string,
  color: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  typographyVariant: PropTypes.string.isRequired,
  className: PropTypes.string,
  message: PropTypes.string.isRequired,
  shadowless: PropTypes.bool,
  extraButton: PropTypes.string,
  extraButtonClick: PropTypes.func
};
