import React, { PureComponent } from 'react';

class App extends PureComponent {
  render() {
    return (
      <div>
        <h1>Basic React Starter Project</h1>
        <h3>Includes:</h3>
        <ul>
          <li>Material-UI</li>
          <li>Redux w/store</li>
        </ul>
      </div>
    );
  }
}

export default App;
