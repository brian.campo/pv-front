import React from 'react';
import PropTypes from 'prop-types';

function Temp(props) {
  const { msg } = props;
  return (
    <div>
      <h2>Temporary component for staging</h2>
      <p>{msg}</p>
    </div>
  );
}

Temp.defaultProps = {
  msg: 'no message'
};

Temp.propTypes = {
  msg: PropTypes.string
};

export default Temp;
