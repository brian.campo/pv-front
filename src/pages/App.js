import React from 'react';
import { Router } from '@reach/router';

import Error from './error/Error';
import Home from './Home';
import PrivateRoute from '../components/auth/PrivateRoute';
import Layout from '../components/Layout/Layout';

function App() {
  return (
    <Router>
      <Home path='/' />
      <PrivateRoute component={Layout} path='app/*' />
      <Error default />
    </Router>
  );
}

App.propTypes = {};

export default App;
